<?php 
//Realizar una expresión regular que detecte emails correctos.

$emailEjemplo = "correo@ejemplo.com";
$emailValido = "/^[A-z0-9.-_]+\@[A-z0-9]+\.([A-z]{2,3})/";

if (preg_match($emailValido, $emailEjemplo)) {
	echo "El correo <b>$emailEjemplo</b> tiene la forma correcta<br>";
} else {
	echo "El correo <b>$emailEjemplo</b> es incorrecto<br>";
}

//Realizar una expresion regular que detecte Curps Correctos
//Que permita usar únicamente estos caracteres: ABCD123456EFGHIJ789

$curpEjemplo = "CURP123456ABCDEF02";
$curpValido = "/(^[A-Z]{4})([0-9]{6})([A-Z]{6})([0-9]{2})/";

if (preg_match($curpValido, $curpEjemplo)) {
	echo "El CURP <b>$curpEjemplo</b> tiene la forma correcta<br>";
} else {
	echo "El CURP <b>$curpEjemplo</b> es incorrecto<br>";
}

//Realizar una expresion regular que detecte palabras de longitud mayor a 50 formadas solo por letras

$longEjemplo = "Electroencefalografista Esternocleidomastoideo Electroencefalográfico";
$longValida = "/^([A-z]{51})/";

if (preg_match($longValida, $longEjemplo)) {
	echo "Se encontraron palabras con una longitud mayor a 50 caracteres en la cadena <b>$longEjemplo</b><br>";
} else {
	echo "No se encontró ninguna palabra con longitud mayor a 50 caracteres  en la cadena <b>$longEjemplo</b><br>";
}

//Crea una funcion para escapar los simbolos especiales.

$stringEjemplo = "Esta\ es una. cadena + con símbolos *especiales.<br>";

echo "$stringEjemplo";

function escapar(&$string) {
	$string = preg_quote($string);
}

escapar($stringEjemplo);

echo "$stringEjemplo";
echo"<br>";

//Crear una expresion regular para detectar números decimales.

$decimalEjemplo = "3.14152";
$valDecimales = "/[0-9]+\.[0-9]+/";
if (preg_match($valDecimales, $decimalEjemplo)) {
	echo "El valor <b>$decimalEjemplo</b> es decimal <br>";
} else {
	echo "El valor <b>$decimalEjemplo</b> no es decimal<br>";
}
?>
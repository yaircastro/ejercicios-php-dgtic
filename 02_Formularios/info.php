<?php 

session_start();

if(empty($_SESSION["usuario"])){
    header("Location: index.php");
}
?>


<!DOCTYPE html>
<html>
<head>
	<title>Información - Ejercicio PHP</title>
	<meta charset="utf-8" content="es">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="estilos.css">
</head>

<body>
	<header>
	</header>

	<nav id="menu">
		<ul>
			<li> <a href="info.php"> Información </a> </li>
			<li> <a href="formulario.php"> Registrar alumnos </a> </li>
			<li> <a href="cerrarsesion.php"> Cerrar sesión </a> </li>
		</ul>
	</nav>

	<main>
	
		<section>
		<div>
		<br/>
		</div>
		
		<div id="info_usuario">
		<p><h3 class="titulo_info">Usuario Autenticado</h3></p>
                <div>
					<p>
					Número de cuenta: 
					<?php echo $_SESSION["alumno"][$_SESSION["usuario"]]["nombre"] . " " . $_SESSION["alumno"][$_SESSION["usuario"]]["primer_apellido"]; ?>
					</p>
                    
					<p>
					Número de cuenta: 
					<?php echo $_SESSION["usuario"]; ?>
					</p>
                    <p>
					Fecha de nacimiento: 
					<?php echo $_SESSION["alumno"][$_SESSION["usuario"]]["fecha_nac"]; ?>
					</p>
                </div>
		</table> 
		<div>
		
		<br/>
		
		<div id="info_guardada">
		<p><h3 class="titulo_info">Usuarios registrados</h3></p>
		<table>
			<thead>
				<th scope="col"> Número de cuenta </th>
				<th scope="col">Nombre completo</th>
				<th scope="col"> Fecha de nacimiento</th>
			</thead>
			<tbody>
				<?php
				foreach ($_SESSION["alumno"] as $key => $value) {
                    echo "<tr>";
                    echo "<th scope='row'>" . $key . "</th>";
                    echo "<td>" . $value["nombre"] . " " . $value["primer_apellido"] . " " . $value["segundo_apellido"] . "</td>";
                    echo "<td>" . $value["fecha_nac"] . "</td>";
					echo "</tr>";
				}
				?>
			</tbody>
		</table> 
		</div>
		
		</section>
		
	</main>

	<footer>
	</footer>
	
</body>
</html>
<!DOCTYPE html>
<head>
	<title>Iniciar Sesión - Ejercicio PHP</title>
	<meta charset="utf-8" content="es">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="estilos.css">
</head>

<body>
	<header>
	</header>

	<main>
	
		<section>
		<div>
		<br/>
		</div>
		<div id="formulario">
			<form name="ingresar" method="POST" action="login.php">
				<h2>Iniciar sesión:</h2>
				<input type="number" name="cuenta" id="cuenta" placeholder="NÚMERO DE CUENTA"></input>
				<input type="password" name="pass" id="pass" placeholder="CONTRASEÑA"></input>
				<p class="centrar"> 
				
				<!--AQUÍ INICIA EL CÓDIGO PHP PARA DESPLEGAR EL ERROR-->
				<?php
				if (isset($_GET["error"])) {
					$error = $_GET["error"];
					if ($error == "invalido") {
						echo "<br><h4>El usuario y/o la contraseña no son válidos. Intente de nuevo.</h4><br>";
					}
					if ($error == "empty") {
						echo "<br><h4>Uno o ambos campos se encuentran vacíos. Intente de nuevo.</h4><br>";
					}
				}
				?>
				<!--AQUÍ TERMINA EL CÓDIGO PHP PARA DESPLEGAR EL ERROR-->
				
				<button type="submit" class="boton1">Iniciar Sesión</button>
				</p>
			</form>
		<div>
		</section>
		
	</main>

<footer>2022 <br/> José Yair Castro Rojas </footer>
</body>
</html> 
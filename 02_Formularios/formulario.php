<?php

session_start();

if(empty($_SESSION["usuario"])){
    header("Location: index.php");
}
?>


<!DOCTYPE html>
<html>
<head>
	<title>Registrar alumno - Ejercicio PHP</title>
	<meta charset="utf-8" content="es">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="estilos.css">
</head>

<body>
	<header>
	</header>

	<nav id="menu">
		<ul>
			<li> <a href="info.php"> Información </a> </li>
			<li> <a href="formulario.php"> Registrar alumnos </a> </li>
			<li> <a href="cerrarsesion.php"> Cerrar sesión </a> </li>
		</ul>
	</nav>

	<main>
	
		<section>
		<div>
		<br/>
		</div>
		<div id="formulario">
			<form name="ingresar" method="POST" action="registrar.php">
				
				<h2>Registrar alumno:</h2>
				<br/>
				<label>Número de cuenta: </label>
				<input type="number" name="cuenta" id="cuenta" placeholder="Número de cuenta" required></input>
				
				<br/>
				<label>Nombre: </label>
				<input type="text" name="nombre" id="nombre" placeholder="Nombre" required></input>
				
				<br/>
				<label>Primer Apellido: </label>
				<input type="text" name="apellido1" id="apellido1" placeholder="Primer Apellido"></input>
				
				<br/>
				<label>Segundo Apellido: </label>
				<input type="text" name="apellido2" id="apellido2" placeholder="Segundo Apellido"></input>

				<br/>
				<label for="genero">Género</label>
				<select name="genero" id="genero"  required>
					<option value="H">Hombre</option>
					<option value="M">Mujer</option>
					<option value="O">Otro</option>
				</select>
				
				<br/>
				<label>Fecha de nacimiento: </label>
				<input type="date" name="fecha_nacimiento" id="fecha_nacimiento" required></input>
				
				<br/>
				<label>Contraseña: </label>
				<input type="password" name="pass" id="pass" required></input>
				
				<p class="centrar"> <button type="submit" class="boton1">Registrar</button> </p>
				
			</form>
		<div>
		</section>
		
	</main>

	<footer>
	</footer>
	
</body>
</html>
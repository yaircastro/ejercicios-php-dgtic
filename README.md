# Ejercicios de PHP para el programa de Ingeniería de Software (DGTIC)

Estas son las actividades para la unidad **Lenguaje de programación PHP** del programa de capacitación en Ingeniería de Software de la DGTIC. 
1. **Triángulo y rombo**  
El programa imprime primero un triángulo centrado y después un rombo centrado, utilizando bucles *for*.
2. **Formularios**  
Se crea un sistema de *login* que permite a los usuarios autenticados registrar nuevos usuarios y ver la información de los usuarios existentes.
3. **Expresiones regulares**  
Se crean expresiones regulares para reconocer correos electrónicos, CURP válidos, números decimales, palabras con una longitud superior a cincuenta caracteres, y una función para escapar caracteres especiales en una cadena de texto.

## Autor
- José Yair Castro Rojas

## Asesores
- Daniel Barajas González
- Hugo German Cuellar Martínez
- Karla Fonseca
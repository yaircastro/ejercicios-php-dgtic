<?php
// apertura de "center" para centrar todo el contenido
echo "<center>";

// Script para generar un triángulo centrado
echo "Esto es un triángulo centrado... </br>";

for ($i = 0; $i <= 10; $i++) {
	for ($j = 1; $j <= $i; $j++) {
		echo "*  ";
	}
	echo "</br>";
}

// Script para generar un rombo

echo "</br></br>...y esto es un rombo centrado </br>";

for($i = 0; $i <= 10; $i++){  
	for($k = 10; $k >= $i; $k--){  
		echo "  ";  
	}  
	for($j = 1;$j <= $i; $j++){  
		echo "*  ";  
	}  
	echo "<br>";
}  
for($i = 9; $i >= 1; $i--){  
	for($k = 10; $k >= $i; $k--){  
		echo "  ";  
	}  
	for($j = 1; $j <= $i; $j++){  
		echo "*  ";  
	}  
	echo "<br>";  
}

// cierre de "center"
echo "</center>";
?>